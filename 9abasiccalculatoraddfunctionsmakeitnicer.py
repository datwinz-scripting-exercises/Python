def add(number1, number2):
    result = number1 + number2
    print(result)

def substract(number1, number2):
    result = number1 - number2
    print(result)

def divide(number1, number2):
    result = number1 / number2
    print(result)

def multiply(number1, number2):
    result = number1 * number2
    print(result)

while True:
    options = input('\nDo you wanna\n1. add\n2. substract\n3. divide\n4. multiply\n5. exit\n')
    match options:
        case '1':
            question = input('What are the two numbers (seperate them with a space)? ')
            question = question.split(' ')
            number_i = question[0]
            number_ii = question[1]
            add(float(number_i), float(number_ii))
        case '2':
            question = input('What are the two numbers (seperate them with a space)? ')
            question = question.split(' ')
            number_i = float(question[0])
            number_ii = float(question[1])
            substract(number_i, number_ii)
        case '3':
            question = input('What are the two numbers (seperate them with a space)? ')
            question = question.split(' ')
            number_i = float(question[0])
            number_ii = float(question[1])
            divide(number_i, number_ii)
        case '4':
            question = input('What are the two numbers (seperate them with a space)? ')
            question = question.split(' ')
            number_i = float(question[0])
            number_ii = float(question[1])
            multiply(number_i, number_ii)
        case '5':
            break
        case _:
            print('Please enter a number between 1 and 5')


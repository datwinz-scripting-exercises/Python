# Python

## Description
Oefenscripts van [Descrypt](https://descrypt.nl/) voor Python.

## License
>"THE BEER-WARE LICENSE" (Revision 42):
><floor.drewes@itvitaelearning.nl> wrote this file. As long as you retain this notice you
>can do whatever you want with this stuff. If we meet some day, and you think
>this stuff is worth it, you can buy me a beer in return Floor Drewes

person1_dict = {'name': 'Henk', 'phone': '0612345678', 'email': 'henk@planet.nl'}
person2_dict = {'name': 'Ingrid', 'phone': '0634567890', 'email': 'ingrid@planet.nl'}
person3_dict = {'name': 'master', 'phone': '0656789012', 'email': 'master@beter.nl'}
phonebook = [person1_dict, person2_dict, person3_dict]

def list_entries():
    for dict in phonebook:
        print(dict)

def add_entry():
    credentials = {'name': '', 'phone': '', 'email': ''}
    question = "What is the person's"
    credentials['name'] = input(f"{question} name? ")
    credentials['phone'] = input(f"{question} phone number? ")
    credentials['email'] = input(f"{question} email? ")
    phonebook.append(credentials)

while True:
    todo = input('What to do?\n1. List entries\n2. Add entry\n3. Exit program\n')
    if todo == "1":
        list_entries()
    elif todo == "2":
        add_entry()
#       list_entries()
    elif todo == '3':
        break
    else:
        print('Please choose number 1, 2 or 3.')


import json
import re

def add(email):
# Checken of de ingevoerde email een email is.
    if re.match(r'\b[A-Za-z0-9._%+-]{1,100}@[A-Za-z0-9.-]{1,100}\.[A-Z|a-z]{2,100}\b',
                email):
# json bestand read/write openen.
        with open('phonebook.json', 'r+', encoding='UTF-8') as phonebook:
            data = json.load(phonebook)
# Checken of de key, oftewel de email al bestaan in de database.
            if email in data:
                print('This email is already in use.')
# Template met persoonsgegevens vaststellen en invullen met user input.
            else:
                credentials = {email:
                               {'name': '',
                                'phone_numbers': {},
                                'emails': {},
                                'adress': {'street': '',
                                           'house_number': '',
                                           'post_code': '',
                                           'place': '',
                                           'country': ''}
                                }
                               }
                gegeven = credentials[email]
# Om naam vragen en checken met regex.
                nameregex = ''
                while nameregex != 'exit':
                    gegeven['name'] = input('Name? ')
                    if re.match(r'\b[A-Za-z0-9._%+-]{1,100}\b',
                                gegeven['name']):
                        nameregex = 'exit'
                    else:
                        print('Please enter a valid name.')
# Om eerste telefoonnummer vragen en checken met regex.
                phone0regex = ''
                while phone0regex != 'exit':
                    gegeven['phone_numbers']['phone0'] = input(
                            'First phone number? '
                            )
                    if re.match(r'\b[0-9]{1,100}\b',
                                gegeven['phone_numbers']['phone0']):
                        phone0regex = 'exit'
                    else:
                        print('Please enter a valid phone number.')
# Om tweede telefoonnummer vragen en checken met regex, niet verplicht.
                phone1regex = ''
                while phone1regex != 'exit':
                    gegeven['phone_numbers']['phone1'] = input(
                            'Second phone number? ')
                    if re.match(r'^$',
                                gegeven['phone_numbers']['phone1']):
                        phone1regex = 'exit'
                    elif re.match(r'\b[0-9]{1,100}\b',
                                gegeven['phone_numbers']['phone1']):
                        phone1regex = 'exit'
                    else:
                        print('Please enter a valid phone number.')
# Om tweede email vragen en checken met regex, niet verplicht.
                email0regex = ''
                while email0regex != 'exit':
                    gegeven['emails']['email0'] = input(
                            'Secondary email? '
                            )
                    if re.match(r'^$',
                                gegeven['emails']['email0']):
                        email0regex = 'exit'
                    elif re.match(r'\b[A-Za-z0-9._%+-]{1,100}@[A-Za-z0-9.-]{1,100}\.[A-Z|a-z]{2,100}\b',
                                gegeven['emails']['email0']):
                        email0regex = 'exit'
                    else:
                        print('Please enter a valid email.')
# Om straat vragen en checken met regex, niet verplicht.
                streetregex = ''
                while streetregex != 'exit':
                    gegeven['adress']['street'] = input('Street? ')
                    if re.match(r'^$',
                                gegeven['adress']['street']):
                        streetregex = 'exit'
                    elif re.match(r'\b\w{1,100}\b',
                                gegeven['adress']['street']):
                        streetregex = 'exit'
                    else:
                        print('Please enter a valid street name.')
# Om huisnummer vragen en checken met regex, niet verplicht.
                house_numberregex = ''
                while house_numberregex != 'exit':
                    gegeven['adress']['house_number'] = input('House number? ')
                    if re.match(r'^$',
                                gegeven['adress']['street']):
                        house_numberregex = 'exit'
                    elif re.match(r'\b[0-9]{1,100}\b',
                                gegeven['adress']['street']):
                        house_numberregex = 'exit'
                    else:
                        print('Please enter a valid house number.')
# Om postcode vragen en checken met regex, niet verplicht.
                post_coderegex = ''
                while post_coderegex != 'exit':
                    gegeven['adress']['post_code'] = input('Postal code? ')
                    if re.match(r'^$',
                                gegeven['adress']['post_code']):
                        post_coderegex = 'exit'
                    elif re.match(r'\b[0-9]{4}[A-Z|a-z]{2}\b',
                                gegeven['adress']['post_code']):
                        post_coderegex = 'exit'
                    else:
                        print('Please enter a valid post code.')
# Om plaats vragen en checken met regex, niet verplicht.
                placeregex = ''
                while placeregex != 'exit':
                    gegeven['adress']['place'] = input('Place? ')
                    if re.match(r'^$',
                                gegeven['adress']['place']):
                        placeregex = 'exit'
                    elif re.match(r'\b\w{1,100}\b',
                                gegeven['adress']['place']):
                        placeregex = 'exit'
                    else:
                        print('Please enter a valid place name.')
# Om land vragen en checken met regex, niet verplicht.
                countryregex = ''
                while countryregex != 'exit':
                    gegeven['adress']['country'] = input('Country? ')
                    if re.match(r'^$',
                                gegeven['adress']['country']):
                        countryregex = 'exit'
                    elif re.match(r'\b\w{1,100}\b',
                                gegeven['adress']['country']):
                        countryregex = 'exit'
                    else:
                        print('Please enter a valid country name.')

# De ingeladen json updaten met de nieuwe persoonsgegevens.
                data.update(credentials)
# De 'cursor' op de goede plek zetten.
                phonebook.seek(0)
# Het eigenlijke json-bestand updaten.
                json.dump(data, phonebook, indent = 3)
    else:
        print('Please enter a valid email.')

if __name__ == "__main__":
    add(input('Email? '))

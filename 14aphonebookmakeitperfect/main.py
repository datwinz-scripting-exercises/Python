# Write a contact managing tool as before, however, this time, store contact data in dicts in a dict. The main dict uses a contact's main email adress as the unique identifier.
# {'<EMAIL>': {'name': <NAME>, 'phone_numbers': <PHONE_NUMBERS>, 'emails': <ALT_EMAILS>}} Dit staat in phonebook.json.
# Adding contacts or displaying a specific contact's data should be done through their email adress.
# Implement a name search as before.
# Allow for writing the main dictionary to a file
# Implement a way to load the main dict.
# Add extra fields to your whishes or liking. Consider the data typing per field (for example, adress: most contact managers support several named adress fields).
import add_entry
import show_entry
import search_entries
import edit_entries

options = ''
while options != 'exit':
    todo = input('What to do?\n1. Show entry by email \n2. Add entry\n3. Search entries by name\n4. Edit entries\n5. Exit program\n')
    if todo == "1":
        show_entry.show(input('Email? '))
    elif todo == "2":
        add_entry.add(input('Email? '))
    elif todo == "3":
        search_entries.search(input('Name? '))
    elif todo == "4":
        edit_entries.edit(input('Email? '))
    elif todo == "5":
        options = 'exit'
    else:
        print('Please choose a valid number')


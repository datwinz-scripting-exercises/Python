import json
import re

def search(name):
# Het json-bestand read-only openen (standaard).
    with open('phonebook.json') as phonebook:
# Het json-bestand inladen als bruikbare data.
        data = json.load(phonebook)
# Kiezen of je met regex (super fuzzy) wil zoeken of niet. Niet is standaard.
        fuzzy = input('Do you wanna fuzzy search? y/N ')
        if fuzzy == "n":
# Print alle persoonsgegevens als de naam precies overeenkomt.
            for value in data.values():
                if dict(value)['name'] == name:
                    print(value)
        elif fuzzy == "y":
# Print alle persoonsgegevens van de namen die voor minimaal 1 letter overeenkomen.
# Zet de gevonden namen eerst in de lijst matches.
# Print daarna alle persoonsgegevens van de gevonden namen.
            matches = []
            for value in data.values():
                value = dict(value)
                nametoregex = value['name']
                match = re.findall(f'.*{name}.*', nametoregex, re.IGNORECASE)
                matches.extend(match)
            for match in matches:
                for value in data.values():
                    value = dict(value)
                    if match == value['name']:
                        print(value)
# Standaard dus precies zoeken.
        else:
            for value in data.values():
                if dict(value)['name'] == name:
                    print(value)

if __name__ == "__main__":
    search(input('Name? '))


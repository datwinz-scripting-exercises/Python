import json

def show(email):
# Het json-bestand read-only openen (standaard).
    with open('phonebook.json') as phonebook:
# Het json-bestand inladen als bruikbare data.
        data = json.load(phonebook)
# De waarde van de opgegeven key, oftewel het mailadres, printen.
        if email in data:
            print(data[email])

if __name__ == "__main__":
    show(input('Email? '))


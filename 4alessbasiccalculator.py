operation = input('Do you wanna add or substract? ')
number1 = input('What is the first number? ')
number2 = input('What is the second number? ')
if operation == "add":
    result = float(number1) + float(number2)
    print(f'{number1} + {number2} = {result}')
elif operation == "substract":
    result = float(number1) - float(number2)
    print(f'{number1} - {number2} = {result}')
else:
    print('Please choose add or substract')


# Een phonebook van dictionaries. Puur wat voorbeeldwaardes.
person1_dict = {'name': 'Henk', 'phone': '0612345678', 'email': 'henk@planet.nl'}
person2_dict = {'name': 'Ingrid', 'phone': '0634567890', 'email': 'ingrid@planet.nl'}
person3_dict = {'name': 'master', 'phone': '0656789012', 'email': 'master@beter.nl'}
phonebook = [person1_dict, person2_dict, person3_dict]

# Entries van phonebook listen. Elke dict is een dictionary met de gegevens van 1 persoon.
def list_entries():
    for dict in phonebook:
        print(dict)

# Vraagt in de while loop met opties voor programma om user input.
def show_entry(userinput):
# Splits de user input in twee delen gedeeld door een spatie:
# Als het eerste deel index is, wil ie ook het indexnummer hebben.
# Als het eerste deel email is, wil ie ook het emailadres in de dict hebben. 
    userinput = userinput.split(' ')
    if userinput[0] == 'index':
        index = int(userinput[1])
# Print de hele dictionary van de gegeven index in de phonebooklist.
        print(phonebook[index])
    elif userinput[0] == 'email':
        for dict in phonebook:
# Checkt of het gegeven emailadres overeen komt met een key email.
# Checkt alle dictionaries in de phonebook list.
# Print daarna de hele dictionary.
            if dict['email'] == userinput[1]:
                print(dict)
    else:
        print('Nonvalid entry. Please type something like "index 0"')

# Vraagt om values voor een nieuwe dictionary voor in de phonebook list.
# Voegt de dictionary aan het eind van de list toe.
def add_entry():
    credentials = {'name': '', 'phone': '', 'email': ''}
    question = "What is the person's"
    credentials['name'] = input(f"{question} name? ")
    credentials['phone'] = input(f"{question} phone number? ")
    credentials['email'] = input(f"{question} email? ")
    phonebook.append(credentials)

# Vraag in de while loop met opties voor programma om user input.
# Doet dit op dezelfde manier al de show_entry() functie.
def remove_entry(userinput):
    userinput = userinput.split(' ')
    if userinput[0] == 'index':
        index = int(userinput[1])
        del phonebook[index]
    elif userinput[0] == 'email':
        for dict in phonebook:
            if dict['email'] == userinput[1]:
                phonebook.remove(dict)
    else:
        print('Nonvalid entry. Please type something like "index 0"')

# De "front end" van het programma. Lust telkens terug naar de opties totdat 5 wordt ingevoerd.
while True:
    todo = input('What to do?\n1. List entries\n2. Show entry\n3. Add entry\n4. Remove entry\n5. Exit program\n')
    if todo == "1":
        list_entries()
    elif todo == "2":
        show_entry(input('You can show by index or by email. Please type index or email, a space and the respective value: '))
    elif todo == "3":
        add_entry()
    elif todo == '4':
        remove_entry(input('You can remove by index or by email. Please type index or email, a space and the respective value: '))
    elif todo == '5':
        break
    else:
        print('Please choose a number between 1 and 5.')


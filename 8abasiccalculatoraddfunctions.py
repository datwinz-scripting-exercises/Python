def add(number1, number2):
    result = number1 + number2
    print(result)

def substract(number1, number2):
    result = number1 - number2
    print(result)

def divide(number1, number2):
    result = number1 / number2
    print(result)

def multiply(number1, number2):
    result = number1 * number2
    print(result)

while True:
    options = input('Do you wanna\n1. add\n2. substract\n3. divide\n4. multiply\n5. exit\n')
    match options:
        case '1':
            number_i = float(input('What is the first number? '))
            number_ii = float(input('What is the second number? '))
            add(number_i, number_ii)
        case '2':
            number_i = float(input('What is the first number? '))
            number_ii = float(input('What is the second number? '))
            substract(number_i, number_ii)
        case '3':
            number_i = float(input('What is the first number? '))
            number_ii = float(input('What is the second number? '))
            divide(number_i, number_ii)
        case '4':
            number_i = float(input('What is the first number? '))
            number_ii = float(input('What is the second number? '))
            multiply(number_i, number_ii)
        case '5':
            break
        case _:
            print('Please enter a number between 1 and 5')


import json
import re

score = 0
jsonfile = open('qandas.json')
qandas = json.load(jsonfile)
for qanda in qandas:
    print(qanda['vraag'])
    print(f"1. {qanda['1']}\n2. {qanda['2']}\n3. {qanda['3']}\n4. {qanda['4']}")
    user_answer = input('Welk antwoord is goed? Geef het nummer: ')
    if re.match(r'\b[0-9]\b', user_answer):
        if user_answer == qanda['correct']:
            score += 1
    else:
        print('Dat is geen nummer, dit antwoord wordt foutgerekend.')
print(f'Je score is: {score}')
print('De juiste antwoorden waren:')
for qanda in qandas:
    print(f"{qanda['vraag']} {qanda[qanda['correct']]}")


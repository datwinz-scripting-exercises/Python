from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

app = Flask(__name__)

app.config['SECRET_KEY'] = 'secretkey'

class NameForm(FlaskForm):
    name = StringField('Username', validators=[DataRequired()])
    submit = SubmitField('Submit')

@app.route('/name/', methods=['GET', 'POST'])
def name():
    name = None
    form = NameForm()
    # Validate Form
    if form.validate_on_submit():
        name = form.name.data
        form.name.data = ''
    return render_template('forms.html', 
                           form = form,
                           name = name)

person1_list = ['Henk', '0612345678', 'henk@planet.nl']
person2_list = ['Ingrid', '0634567890', 'ingrid@planet.nl']
person3_list = ['master', '0656789012', 'master@beter.nl']
phonebook_lists = [person1_list, person2_list, person3_list]
todo = input('What to do?\n1. List entries\n2. Add entry\n')
if todo == "1":
    for list in phonebook_lists:
        print(list)
elif todo == "2":
    counter = 1
    while counter <= 3:
        question = "What is the person's"
        name = input(f"{question} name? ")
        phone = input(f"{question} phone number? ")
        email = input(f"{question} email? ")
        globals()['person'+str(counter)+'_list'][0] = name
        globals()['person'+str(counter)+'_list'][1] = phone
        globals()['person'+str(counter)+'_list'][2] = email
        counter += 1
    for list in phonebook_lists:
        print(list)


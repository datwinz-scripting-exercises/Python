person1_list = ['Henk', '0612345678', 'henk@planet.nl']
person2_list = ['Ingrid', '0634567890', 'ingrid@planet.nl']
person3_list = ['master', '0656789012', 'master@beter.nl']
phonebook_lists = [person1_list, person2_list, person3_list]
options_output = ''
while options_output != 'exit':
    todo = input('What to do?\n1. List entries\n2. Add entry\n3. Exit program\n')
    if todo == "1":
        for list in phonebook_lists:
            print(list)
    elif todo == "2":
        question = "What is the person's"
        name = input(f"{question} name? ")
        phone = input(f"{question} phone number? ")
        email = input(f"{question} email? ")
        credentials = [name, phone, email]
        phonebook_lists.append(credentials)
        for list in phonebook_lists:
            print(list)
    elif todo == '3':
        options_output = 'exit'
    else:
        print('Please choose number 1, 2 or 3.')

